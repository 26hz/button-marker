//redirect when button is clicked
function redirectTo(url) {
	window.location.href = url;
}
//run when all assets on website is ready
$(window).on('load', function(){
	//check if ran on inapp browser
	var userAgent = window.navigator.userAgent.toLowerCase(),
		inapp = /instagram|fbav|fban|kakao|snapchat|line|micromessenger|twitter|wechat/.test(userAgent),
		ios = /iphone|ipod|ipad/.test(userAgent),
		safari = /safari/.test(userAgent),
		android = /android/.test(userAgent),
		chrome = /chrome/.test(userAgent);

	//if inapp browser is detected then the user will be prompted
	if(inapp){
		if(android){
			alert("Please open the URL via Chrome for AR experience")
		} else if(ios){
			alert("Please open the URL via Safari for AR experience")
		}
	} else {
		//if it's detected on ios, but not using safari, then we will open safari
		if(ios && !safari) {
			var location = window.location;
			window.open('x-web-search://?ar', "_self");
			window.location.href = location;
		} else if (android) {
			//if it's on android, but not using chrome then we will open chrome
			if(!chrome){
				window.location.href = 'googlechrome://navigate?url='+ location.href;
			}
		}
	}

	//display socmed buttons when burger button is clicked
	var burger = $(".burger");
	var displayBurgerClicked = $(".displayBurgerClicked");
	var isBurgerClicked = false;
	
	burger.click(function() {
		if(!isBurgerClicked){
			isBurgerClicked = true;
			displayBurgerClicked.css("display", "grid");
		} else {
			isBurgerClicked = false;
			displayBurgerClicked.hide();
		}
	});
	
	var facingCamera = "user";
	if(ios || android){
		facingCamera = "environment";
	}

	var constraints = { 
		video: {
			advanced: [
				{
					facingMode: facingCamera
				}
			]
		} 
	}; 

	var errorCallback = function(error) {
		if (error.name == 'NotAllowedError') {
			alert("Please enable camera for AR experience.\r\nSetting > Browser name > Allow camera")
		}
	};
	navigator.mediaDevices.getUserMedia(constraints)
		.then(null, errorCallback);
});

AFRAME.registerComponent("foo",{
	init:function() {
		var marker = document.querySelector('a-marker');
		var model = document.querySelector('#animated-model');
		var button1 = document.querySelector('#button1');
		
		//hide loader when model is loaded
		model.addEventListener('model-loaded', e => {
			$(".arjs-loader").hide();
		})
		
		function buttonClicked(){
			button1.setAttribute('color', "#FFF");
		}
		marker.addEventListener('markerFound', function() {
			button1.addEventListener('click', buttonClicked);
		});

		marker.addEventListener('markerLost', function() {
			button1.removeEventListener("click", buttonClicked);
		});
	}
});
